import React, { useEffect, useState } from 'react';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';

import './App.css';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { TOKEN_LOCAL_STORAGE } from './constants';
import { Registration } from './components/Registration/Registration';
import Login from './components/Login/Login';
import { loadUser } from './services/UserService';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { fetchAuthors, fetchCourses } from './store/services';
import { useDispatch, useSelector } from 'react-redux';
import { coursesFetched } from './store/courses/actionCreators';
import { authorsFetched } from './store/authors/actionCreators';
import { userLoggedIN } from './store/user/actionCreators';
import { getUser } from './store/selectors';

function App() {
	const dispatch = useDispatch();

	const [loading, setLoading] = useState(true);

	const user = useSelector(getUser);

	useEffect(() => {
		checkUserLoggedIn();
		loadAuthors().then(() => {
			loadCourses();
		});
	}, []);

	const checkUserLoggedIn = async () => {
		const token = localStorage.getItem(TOKEN_LOCAL_STORAGE);
		if (token) {
			await loadCurrentUser();
		}
		setLoading(false);
		console.log('User logged in: ', !!token);
	};

	const loadCourses = async () => {
		try {
			const res = await fetchCourses();
			const obj = await res.json();
			if (res.status === 200) {
				dispatch(coursesFetched(obj.result));
			} else {
				alert(res.statusText);
			}
		} catch (e) {
			alert(e);
		}
	};

	const loadAuthors = async () => {
		try {
			const res = await fetchAuthors();
			const obj = await res.json();
			if (res.status === 200) {
				dispatch(authorsFetched(obj.result));
			} else {
				alert(res.statusText);
			}
		} catch (e) {
			alert(e);
		}
	};

	const loadCurrentUser = async () => {
		try {
			const token = localStorage.getItem(TOKEN_LOCAL_STORAGE);
			const res = await loadUser(token);
			const obj = await res.json();
			if (res.status === 200) {
				const { name, email } = obj.result;
				dispatch(userLoggedIN(name, email, token));
			} else {
				alert('Loading current user failed');
			}
		} catch (err) {
			alert(err);
		}
	};

	if (loading) {
		return null;
	}

	return (
		<div>
			<BrowserRouter>
				<Header />
				<Routes>
					{!user.isAuth && (
						<>
							<Route
								path='/login'
								exact
								element={<Login checkUserLoggedIn={checkUserLoggedIn} />}
							/>
							<Route path='/registration' exact element={<Registration />} />
							<Route path='*' element={<Navigate to='/login' />} />
						</>
					)}
					{user.isAuth && (
						<>
							<Route exact path='/courses' element={<Courses />} />
							<Route path='/courses/:courseId' element={<CourseInfo />} />
							<Route exact path='courses/add' element={<CreateCourse />} />
							<Route path='*' element={<Navigate to='/courses' />} />
						</>
					)}
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;

//import React, { useEffect, useState } from 'react';
// import Header from './components/Header/Header';
// import Courses from './components/Courses/Courses';
// import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';
//
// import './App.css';
// import CreateCourse from './components/CreateCourse/CreateCourse';
// import { TOKEN_LOCAL_STORAGE } from './constants';
// import { Registration } from './components/Registration/Registration';
// import Login from './components/Login/Login';
// import { loadUser } from './services/UserService';
// import CourseInfo from './components/CourseInfo/CourseInfo';
// import { fetchAuthors, fetchCourses } from './store/services';
// import { useDispatch } from 'react-redux';
// import { coursesFetched } from './store/courses/actionCreators';
// import { authorsFetched } from './store/authors/actionCreators';
// import { userLoggedIN, userLoggedOut } from './store/user/actionCreators';
//
// function App() {
// 	const dispatch = useDispatch();
//
// 	const [userLoggedIn, setUserLoggedIn] = useState(null);
//
// 	useEffect(() => {
// 		checkUserLoggedIn();
// 		loadAuthors().then(() => {
// 			loadCourses();
// 		});
// 	}, []);
//
// 	useEffect(() => {
// 		loadCurrentUser();
// 	}, [userLoggedIn]);
//
// 	const loading = true;
//
// 	const hhdhd = () => {
// 		if (storage has token) {
// 			await loadCurrentUser();
// 			loading = fals;
// 		}
// 	}
//
//
// 	const loadCourses = async () => {
// 		try {
// 			const res = await fetchCourses();
// 			const obj = await res.json();
// 			if (res.status === 200) {
// 				dispatch(coursesFetched(obj.result));
// 			} else {
// 				alert(res.statusText);
// 			}
// 		} catch (e) {
// 			alert(e);
// 		}
// 	};
//
// 	const loadAuthors = async () => {
// 		try {
// 			const res = await fetchAuthors();
// 			const obj = await res.json();
// 			if (res.status === 200) {
// 				dispatch(authorsFetched(obj.result));
// 			} else {
// 				alert(res.statusText);
// 			}
// 		} catch (e) {
// 			alert(e);
// 		}
// 	};
//
// 	const loadCurrentUser = async () => {
// 		if (userLoggedIn) {
// 			try {
// 				const token = localStorage.getItem(TOKEN_LOCAL_STORAGE);
// 				const res = await loadUser(token);
// 				const obj = await res.json();
// 				if (res.status === 200) {
// 					const { name, email } = obj.result;
// 					dispatch(userLoggedIN(name, email, token));
// 				} else {
// 					alert('Loading current user failed');
// 				}
// 			} catch (err) {
// 				alert(err);
// 			}
// 		}
// 	};
//
// 	const checkUserLoggedIn = () => {
// 		console.log(
// 			'User logged in: ',
// 			!!localStorage.getItem(TOKEN_LOCAL_STORAGE)
// 		);
// 		setUserLoggedIn(!!localStorage.getItem(TOKEN_LOCAL_STORAGE));
// 	};
//
// 	const logout = () => {
// 		localStorage.removeItem(TOKEN_LOCAL_STORAGE);
// 		dispatch(userLoggedOut());
// 		setUserLoggedIn(false);
// 	};
//
// 	if (userLoggedIn === null) {
// 		return null;
// 	}
//
// 	return (
// 		<div>
// 			<BrowserRouter>
// 				<Header logout={logout} userLoggedIn={userLoggedIn} />
// 				<Routes>
// 					{!userLoggedIn && (
// 						<>
// 							<Route
// 								path='/login'
// 								exact
// 								element={<Login checkUserLoggedIn={checkUserLoggedIn} />}
// 							/>
// 							<Route path='/registration' exact element={<Registration />} />
// 							<Route path='*' element={<Navigate to='/login' />} />
// 						</>
// 					)}
// 					{userLoggedIn && (
// 						<>
// 							<Route exact path='/courses' element={<Courses />} />
// 							<Route path='/courses/:courseId' element={<CourseInfo />} />
// 							<Route exact path='courses/add' element={<CreateCourse />} />
// 							<Route path='*' element={<Navigate to='/courses' />} />
// 						</>
// 					)}
// 				</Routes>
// 			</BrowserRouter>
// 		</div>
// 	);
// }
//
// export default App;
