const url = 'http://localhost:4000';

export const fetchCourses = () => {
	return fetch(`${url}/courses/all`);
};

export const fetchAuthors = () => {
	return fetch(`${url}/authors/all`);
};
