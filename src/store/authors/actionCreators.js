import * as actionTypes from './actionTypes';

export const authorsFetched = (authors) => ({
	type: actionTypes.AUTHORS_FETCHED,
	payload: {
		authors,
	},
});

export const authorAdded = (newAuthor) => ({
	type: actionTypes.AUTHOR_ADDED,
	payload: {
		newAuthor,
	},
});
