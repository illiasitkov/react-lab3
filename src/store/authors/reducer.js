import * as actionTypes from './actionTypes';

const authorsInitialState = [];

export const authorReducer = (authors = authorsInitialState, action) => {
	switch (action.type) {
		case actionTypes.AUTHORS_FETCHED:
			return action.payload.authors;
		case actionTypes.AUTHOR_ADDED:
			return [...authors, action.payload.newAuthor];
		default:
			return authors;
	}
};
