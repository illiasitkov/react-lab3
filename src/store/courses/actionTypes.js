export const COURSES_FETCHED = 'courses/fetched';
export const COURSE_ADDED = 'courses/newAdded';
export const COURSE_DELETED = 'courses/deleted';
