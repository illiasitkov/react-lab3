import * as actionTypes from './actionTypes';

export const userLoggedIN = (name, email, token) => ({
	type: actionTypes.USER_LOGGED_IN,
	payload: {
		name,
		email,
		token,
	},
});

export const userLoggedOut = () => ({
	type: actionTypes.USER_LOGGED_OUT,
});
