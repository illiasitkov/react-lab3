import React from 'react';
import './Button.css';
import PropTypes from 'prop-types';

const Button = ({ buttonText, onClick, type, children, icon }) => (
	<button
		className={icon ? 'icon' : 'fixed-width'}
		type={type}
		onClick={onClick}
	>
		{buttonText}
		{children}
	</button>
);

Button.propTypes = {
	children: PropTypes.element,
	icon: PropTypes.bool,
	buttonText: PropTypes.string,
	onClick: PropTypes.func,
	type: PropTypes.oneOf(['button', 'submit']).isRequired,
};

Button.defaultProps = {
	type: 'button',
	icon: false,
};

export default Button;
