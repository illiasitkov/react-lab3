import React from 'react';
import './Textarea.css';
import PropTypes from 'prop-types';

const Textarea = ({ id, labelText, value, onChange, placeholderText }) => {
	return (
		<div>
			<label htmlFor={id}>{labelText}</label>
			<br />
			<textarea
				id={id}
				value={value}
				onChange={onChange}
				minLength={2}
				required={true}
				rows={4}
				placeholder={placeholderText}
			/>
		</div>
	);
};

Textarea.propTypes = {
	id: PropTypes.string.isRequired,
	labelText: PropTypes.string,
	inputPlaceholder: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.any.isRequired,
};

export default Textarea;
