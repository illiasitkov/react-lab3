import React from 'react';
import './Header.css';
import { Logo } from './components/Logo/Logo';
import { LOGOUT, TOKEN_LOCAL_STORAGE } from '../../constants';
import Button from '../../common/Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import { getUser } from '../../store/selectors';
import { userLoggedOut } from '../../store/user/actionCreators';

const Header = () => {
	const dispatch = useDispatch();

	const user = useSelector(getUser);

	const logout = () => {
		localStorage.removeItem(TOKEN_LOCAL_STORAGE);
		dispatch(userLoggedOut());
	};

	return (
		<header className='d-flex justify-content-between align-items-center flex-wrap'>
			<Logo />
			<div className='d-flex gap-4'>
				{user && <div className='username'>{user.name}</div>}
				{user.isAuth && <Button buttonText={LOGOUT} onClick={logout} />}
			</div>
		</header>
	);
};

export default Header;
