import React, { useState } from 'react';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import Textarea from '../../common/Textarea/Textarea';

import './CreateCourse.css';
import Author from './components/Author/Author';
import { changeHandler } from '../../helpers/changeHandler';
import AddAuthor from './components/AddAuthor/AddAuthor';
import Duration from './components/Duration/Duration';
import AuthorList from './components/ShowAuthors/AuthorList';
import {
	AUTHORS,
	COURSE_AUTHORS,
	DESCRIPTION,
	DESCRIPTION_PLACEHOLDER,
	FILL_FIELDS_ALERT,
	TITLE,
	TITLE_PLACEHOLDER,
} from '../../constants';
import { useNavigate } from 'react-router-dom';
import { v4 } from 'uuid';
import { courseAdded } from '../../store/courses/actionCreators';
import { getCurrentDateFormatted } from '../../helpers/datePipe';
import { useDispatch, useSelector } from 'react-redux';
import { getAuthors } from '../../store/selectors';
import { authorAdded } from '../../store/authors/actionCreators';

const CreateCourse = () => {
	const [authorIds, setAuthorIds] = useState([]);
	const [minutes, setMinutes] = useState(0);
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');

	const authors = useSelector(getAuthors);

	const navigate = useNavigate();
	const dispatch = useDispatch();

	const correctInputs = () => {
		return (
			title && description.length > 1 && minutes > 0 && authorIds.length > 0
		);
	};

	const addNewAuthor = (name) => {
		dispatch(authorAdded({ name, id: v4() }));
	};

	const addNewCourse = (title, description, authors, duration) => {
		dispatch(
			courseAdded({
				title,
				id: v4(),
				description,
				authors,
				duration,
				creationDate: getCurrentDateFormatted(),
			})
		);
	};

	const addCourse = () => {
		if (correctInputs()) {
			addNewCourse(title, description, authorIds, minutes);
			navigate('/courses', { replace: true });
		} else {
			alert(FILL_FIELDS_ALERT);
		}
	};

	const addAuthorToList = (authorId) => {
		setAuthorIds(authorIds.concat(authorId));
	};

	const deleteAuthorFromList = (authorId) => {
		setAuthorIds(authorIds.filter((id) => id !== authorId));
	};

	const authorsToAdd = authors
		.filter((a) => !authorIds.includes(a.id))
		.map((a) => {
			return (
				<div key={a.id} className='mb-2'>
					<Author author={a} onAdd={() => addAuthorToList(a.id)} />
				</div>
			);
		});

	const authorsToDelete = authorIds
		.map((id) => authors.filter((author) => author.id === id)[0])
		.map((a) => {
			return (
				<div key={a.id} className='mb-2'>
					<Author
						deleteMode={true}
						author={a}
						onDelete={() => deleteAuthorFromList(a.id)}
					/>
				</div>
			);
		});

	return (
		<section className='content-wrapper'>
			<div className='mb-4 d-flex align-items-end justify-content-between'>
				<Input
					width='350px'
					type='text'
					value={title}
					onChange={changeHandler(setTitle)}
					id='courseTitle'
					labelText={TITLE}
					inputPlaceholder={TITLE_PLACEHOLDER}
				/>
				<Button onClick={addCourse} buttonText='Create course' />
			</div>
			<div className='mb-4'>
				<Textarea
					id='courseDescription'
					value={description}
					onChange={changeHandler(setDescription)}
					labelText={DESCRIPTION}
					placeholderText={DESCRIPTION_PLACEHOLDER}
				/>
			</div>
			<div className='black-border'>
				<div className='row'>
					<div className='col-6 gap-vertical-lg'>
						<AddAuthor addNewAuthor={addNewAuthor} />
						<Duration setMinutes={setMinutes} minutesValue={minutes} />
					</div>
					<div className='col-6 author-list gap-vertical-md'>
						<AuthorList title={AUTHORS} authorViewList={authorsToAdd} />
						<AuthorList
							title={COURSE_AUTHORS}
							authorViewList={authorsToDelete}
						/>
					</div>
				</div>
			</div>
		</section>
	);
};

export default CreateCourse;
